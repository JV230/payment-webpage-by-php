<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
    <!-- <link rel="text/css" href="style.css"> -->
    <title>Document</title>
</head>

<style>
    body {
        height: 100vh;
        /* background-color: #000; */
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .container {
        width: 400px;
        height: 90vh;
        background: #f0efef;
        border: 1px solid #bdbdbd;
        border-radius: 10px;
        position: relative;

    }




    .search-section {
        width: 100%;
        height: 10%;
        background-color: #023047;
        border-radius: 10px 10px 0 0;
        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    .brand-name {
        color: #fff;
        font-size: 21px;
        margin-left: 18px;
    }

    .search-section input {
        width: 65%;
        border-radius: 5px;
        padding: 6px 0;
        margin-right: 13px;
        border: none;
    }

    .search-section input:focus {
        outline: none;
    }




    .search-section input::-webkit-input-placeholder {
        font-size: 10px;
    }

    .search-section .fa-magnifying-glass {
        position: absolute;
        right: 18px;
        background: #09628e;
        padding: 4px;
        border-radius: 5px;
    }


    .barcode-section {
        display: flex;
        justify-content: center;
        margin-top: -13px;
    }

    .barcode-section .barcode-items {
        margin: 20px;
        color: #f4f4f4;
        display: grid;
        place-items: center;
    }

    .barcode-items span {
        margin-top: 5px;
    }

    .fa-barcode,
    .fa-qrcode {
        color: #f1f1f1;
        font-weight: light;
        font-size: 28px;

    }



    .header {
        width: 100%;
        height: 18.5%;
        background: #023047;

        margin: 0;

    }





    .cashin-icons {
        width: 25px;
        height: 25px;
        border: 2px solid #f1f1f1;


        display: flex;
        justify-content: center;
        align-items: center;
    }

    .cashin-icons .fa-arrow-left {
        font-size: 10px;

    }

    .cashin-icons .fa-arrow-right {
        font-size: 9.5px;
    }



    .amount-login {
        width: 100%;
        height: 40px;

        margin-top: 5px;

        position: relative;
    }

    .amount-section {
        width: 95%;
        height: 90px;
        background: #fff;
        border-radius: 10px;
        box-shadow: 2px 2px 1px inset #fff;

        position: absolute;
        left: 10px;
        top: -50px;

    }


    .btn-illus {
        display: flex;
        justify-content: space-between;
        align-items: center;

        margin-top: -30px;
    }

    .amount-section .welcome-text,
    .amount-section .btn-illus {
        margin-left: 15px;
    }

    .btn-illus button {
        color: #fff;
        background: #095b84;
        border-radius: 5px;
        border: none;
        padding: 7px 10px;
    }

    .btn-illus button .fa-arrow-right-long {
        font-size: 11px;
    }

    .btn-illus img {
        margin-top: -28px;
        margin-right: 10px;
    }


    /* End Amount-Login  */

    .payment-section {
        width: 100%;
        height: 22%;
    }

    .payment-items-containerone,
    .payment-items-containertwo {
        display: flex;
        justify-content: center;
        align-items: center;

        margin: 10px;
        padding: 5px;
    }

    .payment-items {
        width: 25%;
        display: grid;
        place-items: center;
    }

    .payment-items button {
        width: 38px;
        height: 38px;
        border-radius: 100%;
        border: none;
        background: #023047;
        font-size: 20px;
        color: #f1f1f1;
        display: block;
    }

    .payment-items span {
        font-size: 14px;
    }

    /* End Payment-Section  */

    .carousel-section {
        width: 100%;
        height: 100px;
        /* background-color: #095b84; */

    }

    .carousel-container {
        width: 95%;
        height: 80%;
        background: #f0e8e8b4;
        border: 3px solid #f4f4f4;
        border-radius: 10px;

        margin: auto;
    }

    .carousel-one,
    .carousel-two,
    .carousel-three {
        width: 100%;
        height: 100%;
        color: #998d8d;

        display: flex;
        justify-content: center;
        align-items: center;
    }

    .carousel-two,
    .carousel-three {
        visibility: hidden;
    }


    .dot-container {
        display: flex;
        justify-content: center;
        align-items: center;
        margin-top: 3px;
    }

    .dots {
        width: 5px;
        height: 5px;
        background-color: #ddd;
        border-radius: 50%;

        margin: 2px 5px;
    }


    /* End Carousel  */

    .myservic-section {
        width: 100%;
        height: 115px;
        /* background-color: antiquewhite; */
        margin-top: -20px;
    }


    .myservic-container {
        width: 95%;
        height: 110px;
        background-color: #ffff;
        border-radius: 10px;

        margin: auto;
    }

    .myservic-container h4 {
        margin-left: 25px;
        margin-bottom: 8px;
        padding: 8px 0 0 0;
    }

    .services-items-container {

        display: flex;
        justify-content: center;
        align-items: center;


    }

    .services-items {
        width: 25%;
        text-align: center;
        font-size: 12px;
        display: grid;
        place-items: center;
    }


    .services-items button {
        width: 38px;
        height: 38px;
        color: #f1f1f1;
        border-radius: 100%;
        border: none;
        background: #023047;
        font-size: 20px;

        display: flex;
        justify-content: center;
        align-items: center;
    }


    /* End Servic  */

    .menu-section {
        width: 100%;
        height: 8%;
        margin-top: 8px;
        background-color: #f1f1f1;
        border-radius: 0 0 10px 10px;

        border-top: 1px solid #dedede;

        display: flex;
        justify-content: center;
        align-items: center;
    }

    .menu-items-container {
        width: 100%;
        margin-top: 25px;
        display: flex;
        justify-content: center;
        align-items: center;

    }

    .menu-items {
        width: 25%;
        display: grid;
        place-items: center;
    }

    .menu-items i {
        font-size: 20px;
    }

    .menu-items p {
        font-size: 13px;
        margin-top: 3px;
    }

    

    /* register  */
    .registercontainer{
        width: 100%;
        position: absolute;
        left:0;
        top:0;

       display: grid;
       place-items: center;
    }

    .registerimg{
        width: 60px;
        height: 60px;
        background: #bdbdbd;
    }

    .phoneNext{
        width: 100%;
        padding: 5px;
    }
</style>

<body>

    <div class="container homepage">
        <div class="search-section">
            <h3 class="brand-name">HMMPay</h3>
            <input type="search" placeholder="  Enter Keyword to Search">
            <i class="fa-solid fa-magnifying-glass"></i>
        </div>

        <div class="header">


            <div class="barcode-section">

                <div class="barcode-items">
                    <div>
                        <i class="fa-solid fa-barcode"></i>
                    </div>
                    <span>Scan</span>
                </div>

                <div class="barcode-items">
                    <div>
                        <i class="fa-solid fa-qrcode"></i>
                    </div>
                    <span>Receive</span>
                </div>

                <div class="barcode-items">
                    <div class="cashin-icons">
                        <i class="fa-solid fa-dollar-sign"></i>
                        <i class="fa-solid fa-arrow-left"></i>
                    </div>
                    <span>Cash In</span>
                </div>


                <div class="barcode-items">
                    <div class="cashin-icons">
                        <i class="fa-solid fa-dollar-sign"></i>
                        <i class="fa-solid fa-arrow-right"></i>
                    </div>
                    <span>Cash Out</span>
                </div>
            </div>
        </div>

        <!-- End Head  -->

        <div class="amount-login">
            <div class="amount-section">

                <h3 class="welcome-text">Welcome !</h3>
                <div class="btn-illus">
                    <button type="submit" onclick="clickMe()"> Create account or Log In <i
                            class="fa-solid fa-arrow-right-long"></i>
                    </button>
                    <img src="./img/kbzillu.jpg" alt="" width="90px">
                </div>

            </div>
        </div>

        <!-- End Amout-Login  -->

        <div class="payment-section">
            <div class="payment-items-containerone">
                <div class="payment-items">
                    <button type="button"> <i class="fa-solid fa-envelope-open-text"></i></button>
                    <span>Top Up</span>
                </div>

                <div class="payment-items">
                    <button type="button"> <i class="fa-solid fa-money-bill-transfer"></i></button>
                    <span>Transfer</span>
                </div>

                <div class="payment-items">
                    <button type="button"> <i class="fa-regular fa-credit-card"></i></button>
                    <span>Bank Account</span>
                </div>

                <div class="payment-items">
                    <button type="button"> <i class="fa-solid fa-clock-rotate-left"></i></button>
                    <span>History</span>
                </div>

            </div>

            <div class="payment-items-containertwo">
                <div class="payment-items">
                    <button type="button"> <i class="fa-solid fa-file-invoice-dollar"></i></button>
                    <span>Quick Pay</span>
                </div>

                <div class="payment-items">
                    <button type="button"> <i class="fa-solid fa-gift"></i></button>
                    <span>Gift Card</span>
                </div>

                <div class="payment-items">
                    <button type="button"> <i class="fa-solid fa-file-invoice"></i></button>
                    <span>Bill Payment</span>
                </div>

                <div class="payment-items">
                    <button type="button"> <i class="fa-solid fa-location-dot"></i></button>
                    <span>Nearby</span>
                </div>

            </div>
        </div>

        <!-- End Payment-section  -->

        <div class="carousel-section">
            <div class="carousel-container">
                <div class="carousel-one">
                    <div>
                        <i class="fa-solid fa-street-view"></i>
                    </div>
                </div>

                <div class="carousel-two">
                    <div>
                        <i class="fa-solid fa-mountain-city"></i>
                    </div>
                </div>

                <div class="carousel-three">
                    <div>
                        <i class="fa-solid fa-hand-holding-dollar"></i>
                    </div>
                </div>
            </div>

            <div class="dot-container">
                <div class="dots"></div>
                <div class="dots"></div>
                <div class="dots"></div>
            </div>

        </div>


        <!-- End Carousel  -->

        <div class="myservic-section">

            <div class="myservic-container">
                <h4>My Services</h4>
                <div class="services-items-container">
                    <div class="services-items">
                        <button><i class="fa-solid fa-link"></i></button>
                        <p>Wepozt</p>
                    </div>

                    <div class="services-items">
                        <button><i class="fa-solid fa-gifts"></i></button>
                        <p>Gift Card</p>
                    </div>

                    <div class="services-items">
                        <button><i class="fa-solid fa-link"></i></button>
                        <p>HMMPay Market</p>
                    </div>

                    <div class="services-items">
                        <button><i class="fa-solid fa-border-all"></i></button>
                        <p>All</p>
                    </div>
                </div>
            </div>

        </div>

        <!-- End Servic  -->

        <div class="menu-section">
            <div class="menu-items-container">
                <div class="menu-items">
                    <i class="fa-solid fa-house"></i>
                    <p>Home</p>
                </div>

                <div class="menu-items">
                    <i class="fa-solid fa-star-of-david"></i>
                    <p>Life</p>
                </div>

                <div class="menu-items">
                    <i class="fa-solid fa-comment-dots"></i>
                    <p>Message</p>
                </div>

                <div class="menu-items">
                    <i class="fa-solid fa-user"></i>
                    <p>My</p>
                </div>
            </div>

        </div>


        <div class="container registercontainer">

            
            <div class="registerimg">
                <img src="" alt="">
            </div>

            <h3>Register or Login With Your Phone Number</h3>

            <form action="" method="">
                <input type="text" placeholder="09">

                <div>
                    <button type="submit" class="phoneNext">Next</button>
                    <p><a href="">Change Phone Number</a></p>
                </div>
            </form>

            <div>

                <input type="checkbox"><span>By continuing,I agree to the <a href="">Terms of Service</a></span>
            </div>







        </div>





    </div>






    <script>

    </script>



</body>

</html>